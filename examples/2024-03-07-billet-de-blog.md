+++
title = "Exemple de billet de blog"
description = "Description"
authors = [ "Pierre Dupont" ]

insert_anchor_links = "right"

[taxonomies]
# Des tags pour aider à catégoriser les billets
Tags = [ "Site" ]
+++

Ici, c’est le chapô, ce texte sera visible sur la page qui liste les billets.

<!-- more -->

Et ici, après ce commentaire séparateur, c’est le reste du billet

## Partie 1

On peut utiliser du [Markdown](https://www.markdownguide.org/) pour **structurer** et _mettre en forme_ son texte.
