+++
title = "Notre infrastructure à ce jour"
authors = [ "l’équipe d’administration système" ]
updated = "2025-01-21"

insert_anchor_links = "right"

[taxonomies]
Tags = [ "Infrastructure", "Site" ]
+++

Cette page a pour objectif de lister :

- les différents outils utilisés par la communauté ;
- les prestataires auxquels nous faisons appels ;
- les personnes disposants d’accès d’administration.

Ceci peut vous aider à identifier des problèmes rencontrés, et à savoir qui contacter en cas de besoin.

<!-- more -->

### Outils et services

#### Site Bépo

Le site principal du projet Bépo est un MediaWiki.
Il est géré par les membres d’Ergodis et hébergé chez Clever Cloud

La liste des administrateur·ices du Wiki est disponible [sur cette page](https://bepo.fr/wiki/index.php?title=Sp%C3%A9cial:Liste_des_utilisateurs&group=sysop).

Le code déployé est disponible [sur Gitlab](https://gitlab.com/ergodis/web/wiki-bepo).

#### Forum Bépo

Le forum Bépo est un Flarum.
Il est géré par les membres d’Ergodis et hébergé chez Clever Cloud.

La liste des administrateur·ices et modérateur·ices sont disponibles (pour les membres du forum) sur [cette page](https://forum.bepo.fr/users?q=group%3A1) et [cette page](https://forum.bepo.fr/users?q=group%3A4) respectivement.

Le code déployé est disponible [sur Gitlab](https://gitlab.com/ergodis/web/forum-bepo).

#### Site Ergodis

Le site Ergodis (c’est ici) est un site statique. Il est hébergé chez Clever Cloud.

Le contenu est disponible [sur Gitlab](https://gitlab.com/ergodis/web/site-ergodis), il est déployé depuis Gitlab directement à chaque mise à jour du dépôt.

La liste des membres qui peuvent modifier le dépôt est disponible [sur cette page](https://gitlab.com/ergodis/web/site-ergodis/-/project_members?sort=access_level_desc).

Il est possible de proposer une contribution via une _Requête de fusion_[^1]

#### Listes de diffusion

Les listes de diffusion Ergodis (Discussion et Collège) sont gérées par TuxFamily

La liste des modérateur·ices de la liste de diffusion Discussion est la suivante :

- Arathor
- Chouhartem
- Flavien21
- Miltøn
- Mimoza
- Zluglu

La liste des modérateur·ices de la liste de diffusion Collège est la suivante :

- Arathor
- Chouhartem
- Flavien21
- Morgane “Sardem FF7” Glidic

#### Page HelloAsso

Ergodis est présente [sur HelloAsso](https://www.helloasso.com/associations/ergodis) pour la gestion des inscriptions et cotisations.

La liste des personnes avec les accès d’administration est la suivante :

- Arathor
- Flavien21
- Malekpe
- Miltøn
- Morgane “Sardem FF7” Glidic
- Le ou la trésorière d’Ergodis via un email dédié

#### Dépôts Git

Les dépôts Git du projet Bépo sont hébergés sur Gitlab.com.

La liste des membres (dont les propriétaires du groupe) est disponible [sur cette page](https://gitlab.com/groups/bepo/-/group_members?sort=access_level_desc)

Les dépôts Git d’Ergodis sont hébergés sur Gitlab.com.

La liste des membres (dont les propriétaires du groupe) est disponible [sur cette page](https://gitlab.com/groups/ergodis/-/group_members?sort=access_level_desc)

#### Canaux IRC

Outre les listes de diffusion, le moyen de communication principal et historique d’Ergodis est IRC.
Le réseau IRC choisi par Ergodis est Libera.chat
Deux canaux IRC sont gérés par les membres de Ergodis : `#bepo` et `#ergodis`.

`#bepo` est utilisé pour les discussions générales autour du projet Bépo.

`#ergodis` est utilisé pour les Assemblées Générales de l’association.

La liste des opérateur·ices des canaux (mode `+A`) et celle des utilisateur·ices ayant le droit de parole (mode `+V`) en cas de passage des canaux
en mode modération est disponible via la commande `FLAGS` de ChanServ (e.g. `/msg ChanServ flags #bepo`).

#### Groupe Matrix

Par praticité, un groupe Matrix `#bepo` est également disponible sur [#bepo:matrix.org](https://matrix.to/#/#bepo:matrix.org).
Ce groupe est ponté avec le canal IRC `#bepo`.

La liste des membres et de leurs droits est disponible via le client Matrix de votre choix.

Le pont entre IRC et Matrix est géré par Morgane “Sardem FF7” Glidic.

#### Serveur Discord

La communauté du projet Bépo a également ouvert un serveur Discord pour atteindre une audience connaissant peu ou pas IRC.

Le canal IRC `#ergodis` est ponté sur Discord pour permettre aux membres de l’association uniquement présent sur Discord d’assister et de participer aux AG.

La liste des membres et de leurs droits est disponible dans l’interface de Discord.

#### Noms de domaines et DNS

Les noms de domaines enregistrés par Ergodis sont :

- `bepo.fr` et `bépo.fr` ;
- `ergodis.org` et `ergodis.fr`.

Les serveurs DNS pour tous les domaines d’Ergodis sont gérés par Cloudflare.


### Prestataires

#### TuxFamily

Hébergeur associatif en PaaS[^2]/DBaaS[^3]/SaaS[^4].

C’était l’hébergeur historique des différents outils du projet Bépo.
La décision a été prise de migrer vers Clever Cloud après plusieurs indisponibilités chez TuxFamily, la gestion des services chez TF étant purement bénévole.

Les listes de diffusion sont toujours gérées par TuxFamily.

Les personnes ayant des accès sur le project Bépo chez TuxFamily sont :

- fredb
- asr
- flocon
- dhalsim
- ariasuni
- xavierc
- chili
- thblt
- A2
- Arathor
- Chouhartem
- Cyxae
- Flavien21
- GilDev
- Glehmann
- Ishido
- JoFo
- Malekpe
- Miltøn
- Mimoza
- Morgane “Sardem FF7” Glidic
- Nemolivier
- Tazzon
- Zluglu

Spécifiquement sur la liste de diffusion Discussion :

- Arathor
- Chouhartem
- Flavien21
- Miltøn
- Mimoza
- Morgane “Sardem FF7” Glidic
- Zluglu

Spécifiquement sur la liste de diffusion Collège :

- Arathor
- Chouhartem
- Flavien21
- Morgane “Sardem FF7” Glidic

#### Clever Cloud

Hébergeur commercial en PaaS[^2]/DBaaS[^3].

Ils offrent à Ergodis l’hébergement depuis fin 2023.

Les personnes ayant des accès sur l’organisation Ergodis chez Clever Cloud sont :

- Arathor
- Cyxae
- Morgane “Sardem FF7” Glidic

#### Gitlab.com

Hébergeur en SaaS[^4] et éditeur logiciel.

L’utilisation de Gitlab a été décidée pour éviter un grossissement trop important des données hébergées par TuxFamily (dont l’usage est soumis à un quota) et faciliter le gestion des droits pour les personnes souhaitant participer au développements.

Les listes des membres (dont les propriétaires des groupes) sont disponibles [sur cette page](https://gitlab.com/groups/bepo/-/group_members?sort=access_level_desc) et [sur celle-ci](https://gitlab.com/groups/ergodis/-/group_members?sort=access_level_desc).

#### HelloAsso

Hébergeur en SaaS[^4].

HelloAsso est une plateforme de gestion des adhésions, cotisations et dons pour associations.

La liste des personnes ayant les accès d’administration se trouve [plus haut sur cette page](#Page_HelloAsso).

#### Infomaniak

Hébergeur commercial et registrar.

Ergodis a enregistré ses noms de domaines chez Infomaniak :

- `bepo.fr` et `bépo.fr`
- `ergodis.org` et `ergodis.fr`

Le service d’email est également utilisé pour le wiki et le forum (notamment pour l’envoi des emails de reset de mots de passe).

Les personnes ayant des accès sur l’organisation Ergodis chez Infomaniak sont :

- Arathor
- Morgane “Sardem FF7” Glidic

#### Cloudflare

Les personnes ayant des accès sur l’organisation Ergodis chez Cloudflare sont :

- Arathor
- les membres du Collège via un compte partagé
- Morgane “Sardem FF7” Glidic


[^1]: _Merge Request_ en anglais : action de demander la fusion de modifications sur un dépôt Git
[^2]: _Platform as a Service_ : modèle d’hébergement où la gestion du système est assurée par le prestataire et le client n’a plus qu’à y déposer son application
[^3]: _Database as a Service_ : modèle d’hébergement où la gestion du système et du logiciel de base de données est assurée par le prestataire et le client n’a plus qu’à utiliser la base de données
[^4]: _Software as a Service_ : modèle d’hébergement où la gestion complète du logiciel est assurée par le prestataire et le client ne fait que l’utiliser
