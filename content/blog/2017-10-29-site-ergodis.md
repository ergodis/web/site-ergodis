+++
title = "Nouveau site Ergodis"
authors = [ "GilDev" ]

insert_anchor_links = "right"

[taxonomies]
Tags = [ "Site" ]

[extra]
+++

L’adresse [ergodis.org](https://ergodis.org) renvoyait jusqu’à présent sur le [wiki Bépo](https://bepo.fr/). Je me suis proposé pour réaliser un _petit_ site vitrine afin de présenter l’association et ses projets en quelques mots. On y trouve également tous les moyens de contacts et les réseaux sociaux de l’association, ainsi que sa constitution.

<!-- more -->

Ce petit blog est aussi un moyen de partager des informations, je pense aux comptes-rendus d’assemblée générale par exemple.

Pour toute remarque, je vous invite à [me contacter](mailto:gildev@gmail.com) !
