+++
title = "Nouveau site Ergodis, le retour"
authors = [ "Ergodis" ]

insert_anchor_links = "right"

[taxonomies]
Tags = [ "Site" ]

[extra]
+++

Cette fois-ci, c’est la bonne ! Voilà un site tout neuf pour espérer faire renaître la motivation parmi la communauté.

<!-- more -->

Ça n’aura échappé à personne, les informations du site commençaient à dater.
Même si on ne peut dire que la motivation coule à flots, les limitations sur l’accès à l’édition du site ne facilitaient pas la tâche.

On garde l’esprit d’un site simple et léger, et on passe sur une génération purement statique.
Les ajouts se feront via Git, par exemple en faisant en Merge Request sur Gitlab, et le site sera déployé automatiquement à la suite.

