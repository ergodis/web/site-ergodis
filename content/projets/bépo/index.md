+++
title = "La disposition Bépo"
insert_anchor_links = "right"
description = "Le Bépo est une disposition de clavier ergonomique principalement orientée vers l’écriture de textes français."
+++

Le [Bépo](https://bepo.fr/) est une disposition de clavier ergonomique principalement orientée vers l’écriture de textes français.

<!-- more -->

Les intérêts du Bépo, similaires à ceux de la disposition ergonomique américaine Dvorak, sont nombreux :

* une réduction des troubles musculo-squelettiques causés par une utilisation importante du clavier ;
* un placement logique et pratique de certains caractères (parenthèses placées côte à côte, caractères de ponctuation accessibles…) ;
* une répartition optimisée de l’utilisation des doigts (alternance main gauche-main droite, optimisation de la rangée de repos) ;
* un accès aux caractères nécessaires pour une typographie française correcte ainsi qu’aux caractères de langues étrangères.

Le Bépo est désormais normalisé par l’[AFNOR](https://www.afnor.org/) dans sa version 1.1.
