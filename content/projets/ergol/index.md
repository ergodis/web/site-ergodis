+++
title = "La disposition Ergo‑L"
insert_anchor_links = "right"
description = "Ergo‑L est une disposition de clavier ergonomique optimisée pour le français, l’anglais et le code. La première disposition francophone conçue pour tous les claviers !"
+++

[Ergo‑L](https://ergol.org/) est une disposition de clavier ergonomique
optimisée pour le français, l’anglais et le code. La première disposition
francophone conçue pour tous les claviers !

<!-- more -->

Si Dvorak a été pensé pour les machines à écrire mécaniques des années 1930, des
dispositions comme Colemak ont été conçues dans les années 2000 spécifiquement
pour les claviers informatiques. Colemak est la plus connue ; Ergo‑L en est une
adaptation francophone.

Les principaux avantages d’Ergo‑L sont :

* une **réelle** réduction des troubles musculo-squelettiques causés par une
  utilisation importante du clavier ;
* plus optimisée que Bépo pour le français ;
* plus optimisée que Dvorak pour l’anglais ;
* préservation des raccourcis de main gauche (<kbo>Ctrl</kbd> -
  <kbd>A</kbd> <kbd>S</kbd> <kbd>Z</kbd> <kbd>X</kbd> <kbd>C</kbd> <kbd>V</kbd>) ;
* une couche AltGr, optionnelle, entièrement dédiée aux symboles de
  programmation ;
* une compatibilité parfaite avec tous les claviers, même les claviers
  ergonomiques les plus compacts.

Tout comme Bépo, Ergo‑L permet une typographie soignée en français et dans de
nombreuses langues étrangères.
