+++
title = "Bienvenue sur le site de l’association Ergodis !"
description = "Le site de l’association Ergodis"
insert_anchor_links = "right"
template = "homepage.html"
+++

{% homepage_insert() %}
**Ergodis** est une association visant à _promouvoir_ une meilleure _ergonomie_ des dispositifs de saisie, de pointage, et plus généralement du _poste de travail_ informatique.

Elle s’attache aussi au bon respect des _règles_ de typographie des _langues_ pratiquées par ses adhérents.
{% end %}

Les moyens utilisés comprennent, mais ne se limitent pas à :

* la promotion d’une disposition de clavier alternative à l’AZERTY ou au QWERTY ;
* la promotion de la dactylographie ;
* la promotion d’une typographie française soignée ;
* l’adaptation des outils informatiques utilisés sur différents supports (ordinateurs, tablettes, terminaux mobiles…).
