+++
title = "Nous contacter"

insert_anchor_links = "right"
+++

Vous trouverez ici les moyens pour contacter Ergodis et ses membres.

<!-- more -->

Il est possible de nous contacter [par courriel](mailto:contact@ergodis.org).

La communauté Ergodis se réunit et discute grâce à plusieurs outils :
* un [forum](https://forum.bepo.fr)
* un [serveur Discord](https://discord.gg/ymt5uCTudW)
* une [liste de diffusion](https://listengine.tuxfamily.org/ergodis.org/discussions/)
* un [canal IRC](https://web.libera.chat/#ergodis) sur le réseau [Libera.Chat](https://libera.chat/)

La liste de diffusion (souvent abrégée ML, pour <i lang="en">mailing list</i>) est le canal de discussion historique de la communauté. Elle reste utilisée par les anciens, donc il est recommandé d’y être inscrit si on veut suivre les conversations importantes.
Cependant, les nouveaux utilisateurs lui préfèrent le forum.

Pour les conversations instantanées, historiquement ça se passait sur IRC, et il est toujours possible de venir pour discuter en petit comité avec les membres actifs d’Ergodis.
Mais là aussi, les nouveaux utilisateurs préfèrent Discord.
