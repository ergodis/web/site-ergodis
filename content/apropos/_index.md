+++
title = "À propos"
template = "cards.html"

insert_anchor_links = "right"
+++

Ergodis est une _association loi de 1901_ initialement créée par la communauté de la disposition Bépo afin de gagner en crédibilité,
notamment en vue d’effort de normalisation et d’adoption par les acteurs industriels.

Vous pouvez adhérer à l’association via [notre page HelloAsso](https://www.helloasso.com/associations/ergodis)
pour l’année en cours (ou la suivante en fin d’année).

## Documents
