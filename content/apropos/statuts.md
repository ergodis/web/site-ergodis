+++
title = "Statuts d’Ergodis"
description = "Vous trouverez ici les statuts de l’association"

insert_anchor_links = "right"
+++

Vous trouverez ici les statuts de l’association

<!-- more -->

Reproduction complète :

{{ document(name="statuts") }}
